Introduction
============
Remt project is note-taking support tool for reMarkable tablet

    https://remarkable.com/

Features
--------
The features of Remt are

- reMarkable tablet operations

  - list files and directories
  - create directories
  - export a notebook as PDF file using reMarkable tablet renderer or
    Remt project renderer
  - export an annotated PDF document using reMarkable tablet renderer or
    Remt project renderer
  - import a PDF document
  - create index of PDF file annotations
  - interactive selection of files on a tablet using `fzf` command

- cache of reMarkable tablet metadata to speed up tablet operations;
  caching supports multiple devices
- Remt project renderer

  - supports export of large files and usually produces smaller PDF files
    comparing to the reMarkable tablet renderer
  - multi-page notebooks and PDF files
  - all drawing tools

- Remt project can be used as a library for UI applications

Known Bugs
----------

1. Brushes should imitate reMarkable tablet output more closely.
2. Import of a PDF file requires restart of reMarkable tablet.

.. vim: sw=4:et:ai
