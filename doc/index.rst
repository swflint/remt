Remt - Note-taking Support Tool
===============================
Remt project is note-taking support tool for reMarkable tablet

    https://remarkable.com/

The main features of the tool are import of PDF documents, export of PDF
documents and notebooks, and PDF file annotation index.

This project is *not* an official project of the reMarkable company.

Remt project is licensed under terms of `GPL license, version 3
<https://www.gnu.org/licenses/gpl-3.0.en.html>`_. As stated in the license,
there is no warranty, so any usage is on your own risk.

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2

   intro
   install
   cmd
   workflow
   troubleshoot

* :ref:`genindex`
* :ref:`search`

.. vim: sw=4:et:ai
