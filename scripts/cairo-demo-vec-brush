#!/usr/bin/env python3
#
# remt - note-taking support tool for reMarkable tablet
#
# Copyright (C) 2018-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Draw shapes with vector patterns using Cairo.

Zoom-in the output file as much as possible and the quality of the picture
is not degrading.
"""

import cairo

# create pattern using Cairo
sf_vec = cairo.RecordingSurface(cairo.Content.COLOR_ALPHA, (0, 0, 10, 10))
cr_vec = cairo.Context(sf_vec)
cr_vec.set_line_width(0.5)
cr_vec.move_to(0, 0)
cr_vec.line_to(10, 10)
cr_vec.stroke()

brush = cairo.SurfacePattern(sf_vec)
brush.set_extend(cairo.EXTEND_REPEAT)

# draw shapes with the pattern
surface = cairo.PDFSurface('cairo-demo-vec-brush.pdf', 1000, 1000)
cr = cairo.Context(surface)
cr.set_source(brush)

# empty rectangle
cr.set_line_width(30)
cr.rectangle(20, 20, 160, 160)
cr.stroke()

# filled rectangle
cr.set_line_width(30)
cr.rectangle(820, 820, 160, 160)
cr.fill()
 
# crossed lines
# note: should we thing about orientation?
cr.set_line_width(20)
cr.move_to(400, 400)
cr.line_to(600, 600)
cr.move_to(600, 400)
cr.line_to(400, 600)
cr.stroke()

surface.finish()

# vim: sw=4:et:ai
