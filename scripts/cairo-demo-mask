#!/usr/bin/env python3
#
# remt - note-taking support tool for reMarkable tablet
#
# Copyright (C) 2018-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Draw shapes with mask using Cairo.
"""

import cairo

# draw mask surface with background pattern
mask_surface = cairo.RecordingSurface(cairo.Content.COLOR_ALPHA, (0, 0, 100, 100))
mask_cr = cairo.Context(mask_surface)

for k in range(3, 100, 5):
    mask_cr.set_source_rgba(0.5, 0.5, 0.5, 0.5)
    mask_cr.set_line_width(0.5)
    mask_cr.move_to(k, 0)
    mask_cr.line_to(k, 100)
    mask_cr.move_to(0, k)
    mask_cr.line_to(100, k)
    mask_cr.stroke()

# draw shapes with a color onto source surface
src_surface = cairo.RecordingSurface(cairo.Content.COLOR_ALPHA, (0, 0, 100, 100))
src_cr = cairo.Context(src_surface)

src_cr.set_source_rgba(1, 0, 0, 1)
src_cr.set_line_width(20)
src_cr.move_to(25, 5)
src_cr.line_to(25, 95)
src_cr.stroke()

src_cr.set_line_width(20)
src_cr.set_source_rgba(0, 0, 1, .5)
src_cr.move_to(50, 5)
src_cr.line_to(50, 95)
src_cr.stroke()

for i in range(3):
    src_cr.set_source_rgba(0, 1, 0, .2)
    src_cr.set_line_width(20)
    src_cr.move_to(75 + i * 5, 5)
    src_cr.line_to(75 + i * 5, 95)
    src_cr.stroke()

# create output PDF file
surface = cairo.PDFSurface('cairo-demo-mask.pdf', 100, 100)
cr = cairo.Context(surface)

# draw shapes using source surface and use mask surface to render pattern
cr.set_source_surface(src_surface)
cr.mask_surface(mask_surface)

surface.finish()

# vim: sw=4:et:ai
