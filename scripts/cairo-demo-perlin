#!/usr/bin/env python3
#
# remt - note-taking support tool for reMarkable tablet
#
# Copyright (C) 2018-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Draw Perlin noise with Cairo library.

The Perlin noise implementation is copied from

    https://stackoverflow.com/questions/42147776/producing-2d-perlin-noise-with-numpy

"""

import cairo
import math
import numpy as np
from skimage import measure
from itertools import product

SIZE = 100
SCALE = 20  # the higher value, the finer shape, try 5, 10, 20, 40
CUTOFF = 0.5

def perlin(x, y, seed=0):
    # permutation table
    np.random.seed(seed)
    p = np.arange(256, dtype=int)
    np.random.shuffle(p)
    p = np.stack([p, p]).flatten()
    # coordinates of the top-left
    xi = x.astype(int)
    yi = y.astype(int)
    # internal coordinates
    xf = x - xi
    yf = y - yi
    # fade factors
    u = fade(xf)
    v = fade(yf)
    # noise components
    n00 = gradient(p[p[xi] + yi], xf, yf)
    n01 = gradient(p[p[xi] + yi + 1], xf, yf - 1)
    n11 = gradient(p[p[xi + 1] + yi + 1], xf - 1, yf - 1)
    n10 = gradient(p[p[xi + 1] + yi], xf - 1, yf)
    # combine noises
    x1 = lerp(n00, n10, u)
    x2 = lerp(n01, n11, u) # FIX1: I was using n10 instead of n01
    return lerp(x1, x2, v) # FIX2: I also had to reverse x1 and x2 here

def lerp(a, b, x):
    "linear interpolation"
    return a + x * (b - a)

def fade(t):
    "6t^5 - 15t^4 + 10t^3"
    return 6 * t ** 5 - 15 * t ** 4 + 10 * t ** 3

def gradient(h, x, y):
    "grad converts h to the right gradient vector and return the dot product with (x, y)"
    vectors = np.array([[0, 1], [0, -1], [1, 0], [-1, 0]])
    g = vectors[h % 4]
    return g[:,:,0] * x + g[:,:,1] * y

lin = np.linspace(0, SCALE, SIZE, endpoint=False)
vec_x, vec_y = np.meshgrid(lin, lin)
result = perlin(vec_x, vec_y, seed=2)

surface = cairo.PDFSurface('cairo-demo-perlin.pdf', SIZE - 1, SIZE - 1)
cr = cairo.Context(surface)

# normalize to [0, 1]
result = (result + 1) * 0.5
contours = measure.find_contours(result, CUTOFF)
for c in contours:
    cr.set_source_rgb(0, 0, 0)
    cr.set_line_width(0.1)
    cr.new_path()
    for y, x in c:
        cr.line_to(x, y)
    cr.fill()

surface.finish()

# vim: sw=4:et:ai
